import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MLaptopRoutingModule } from './m-laptop-routing.module';
import { LenovoComponent } from './pages/lenovo/lenovo.component';
import { DellComponent } from './pages/dell/dell.component';
import { AsusComponent } from './pages/asus/asus.component';


@NgModule({
  declarations: [
    LenovoComponent,
    DellComponent,
    AsusComponent
  ],
  imports: [
    CommonModule,
    MLaptopRoutingModule
  ]
})
export class MLaptopModule { }
