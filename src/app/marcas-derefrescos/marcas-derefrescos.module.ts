import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcasDerefrescosRoutingModule } from './marcas-derefrescos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MarcasDerefrescosRoutingModule
  ]
})
export class MarcasDerefrescosModule { }
