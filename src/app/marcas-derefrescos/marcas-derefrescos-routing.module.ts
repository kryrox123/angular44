import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocaColaComponent } from '../marcas-derefresco/pages/coca-cola/coca-cola.component';
import { FantaComponent } from '../marcas-derefresco/pages/fanta/fanta.component';
import { SpriteComponent } from '../marcas-derefresco/pages/sprite/sprite.component';

const routes: Routes = [
  {path:"",
  children:[
    {path:"coca-cola",component:CocaColaComponent},
    {path:"fanta",    component:FantaComponent},
    {path:"sprite",   component:SpriteComponent},
    {path:"**",redirectTo:"fanta"}
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcasDerefrescosRoutingModule { }
