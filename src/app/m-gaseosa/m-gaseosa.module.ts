import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MGaseosaRoutingModule } from './m-gaseosa-routing.module';
import { CocaColaComponent } from './pages/coca-cola/coca-cola.component';
import { FantaComponent } from './pages/fanta/fanta.component';
import { SpriteComponent } from './pages/sprite/sprite.component';


@NgModule({
  declarations: [
    CocaColaComponent,
    FantaComponent,
    SpriteComponent
  ],
  imports: [
    CommonModule,
    MGaseosaRoutingModule
  ]
})
export class MGaseosaModule { }
