import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocaColaComponent } from './pages/coca-cola/coca-cola.component';
import { FantaComponent } from './pages/fanta/fanta.component';
import { SpriteComponent } from './pages/sprite/sprite.component';

const routes: Routes = [
  {
    path: "",
    children: [
      { path: "cocacola", component: CocaColaComponent },
      { path: "fanta", component: FantaComponent },
      { path: "sprite", component: SpriteComponent },
      { path: "**", redirectTo: "cocacola" }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MGaseosaRoutingModule { }
