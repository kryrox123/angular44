import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArquitecturaComponent } from './pages/arquitectura/arquitectura.component';
import { MedicinaComponent } from './pages/medicina/medicina.component';
import { SistemasComponent } from './pages/sistemas/sistemas.component';


const routes: Routes = [
  {path:"",
  children:[
    {path:"medicina", component:MedicinaComponent},
    {path:"sistemas", component:SistemasComponent},
    {path:"arquitectura", component:ArquitecturaComponent},
    {path:"**",redirectTo:"medicina"}

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarrerasRoutingModule { }
