import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CocaColaComponent } from './marcas-derefresco/pages/coca-cola/coca-cola.component';
import { FantaComponent } from './marcas-derefresco/pages/fanta/fanta.component';
import { SpriteComponent } from './marcas-derefresco/pages/sprite/sprite.component';

@NgModule({
  declarations: [
    AppComponent,
    CocaColaComponent,
    FantaComponent,
    SpriteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
