import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DfrontEndComponent } from './dfront-end.component';

describe('DfrontEndComponent', () => {
  let component: DfrontEndComponent;
  let fixture: ComponentFixture<DfrontEndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DfrontEndComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DfrontEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
