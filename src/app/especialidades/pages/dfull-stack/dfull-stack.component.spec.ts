import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DfullStackComponent } from './dfull-stack.component';

describe('DfullStackComponent', () => {
  let component: DfullStackComponent;
  let fixture: ComponentFixture<DfullStackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DfullStackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DfullStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
