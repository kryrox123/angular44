import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbackendComponent } from './dbackend.component';

describe('DbackendComponent', () => {
  let component: DbackendComponent;
  let fixture: ComponentFixture<DbackendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbackendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DbackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
