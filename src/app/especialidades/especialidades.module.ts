import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DFrondEndComponent } from './page/dfrond-end/dfrond-end.component';
import { DbackendComponent } from './page/dbackend/dbackend.component';
import { DfullstackComponent } from './page/dfullstack/dfullstack.component';
import { EspecialidadesRoutingModule } from './especialidades-routing.module';



@NgModule({
  declarations: [
    DFrondEndComponent,
    DbackendComponent,
    DfullstackComponent
  ],
  imports: [
    CommonModule,
    EspecialidadesRoutingModule
 ]
})
export class EspecialidadesModule { }
