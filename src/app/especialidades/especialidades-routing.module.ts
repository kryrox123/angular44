import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  RouterModule, Routes } from '@angular/router';
import { DbackendComponent } from './page/dbackend/dbackend.component';
import { DFrondEndComponent } from './page/dfrond-end/dfrond-end.component';
import { DfullstackComponent } from './page/dfullstack/dfullstack.component';


const routes : Routes = [
  {
    path:"",
    children:[
      {path:"dbackend",component: DbackendComponent },
      {path:"dfrond-end",component:DFrondEndComponent},
      {path:"dfullstack",component:DfullstackComponent},
      {path:"**",redirectTo: "dbackend"}
    ]
    
  }

];

@NgModule({

  imports: [
    RouterModule.forChild(routes)

  ]
})
export class EspecialidadesRoutingModule { }
